#ifndef MENU_H
#define MENU_H

#include <SFML/Graphics.hpp>

#include "Application.h"
#include "ApplicationJeu.h"
#include "ApplicationEditeur.h"

class Boutons;

class Menu : public Application
{
public:
    Menu();
    ~Menu();

private:
    void loop()  override;

    //ELEMENTS GRAPHIQUES//
    sf::Sprite m_logo;
    sf::Texture m_tex_logo;
    std::vector<Boutons *> m_boutons;
    void dessiner_boutons();

    //FONCTIONS EVENEMENTS//
    void mouse_button_pressed()                        override;
    //void mouse_button_released()                       override;
    //void mouse_moved()                                 override;
    //void key_pressed(const sf::Event::KeyEvent &event) override;

};

#endif // MENU_H
