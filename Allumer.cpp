#include "Allumer.h"

Allumer::Allumer(Personnage *perso, Plateau *plateau)
    :m_perso(perso),
      m_plateau(plateau)
{}

Allumer::~Allumer()
{}

//PERMET L'EXECUTION DE L'ACTION ALLUMER EN FOCNTION DE LA CASE CONTENANT LE PERSONNAGE
void Allumer::executerAction()
{
    m_plateau -> recupererCasePerso(m_perso) -> allumerCase();
}
