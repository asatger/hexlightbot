#ifndef CASES_H
#define CASES_H

#include <SFML/Graphics.hpp>
#include <iostream>

#include "EtatCase.h"

using Position = sf::Vector2f;

class Cases
{
public:
    //CONSTRUCTEUR & DESTRUCTEUR//
    Cases(Position position, EtatCase::Etat etat);
    ~Cases();

    //FONCTIONS MEMBRES//
    void afficher_case(sf::RenderWindow & window);
    void allumerCase();
    void estVisitee();

    //FONCTIONS SETTER//
    void setEtat(EtatCase::Etat etat);

    //FONCTIONS GETTER//
    Position getPos();
    EtatCase::Etat getEtat();
    sf::CircleShape getCircle();

private:
    sf::CircleShape m_hexa;
    Position m_position;
    sf::Color m_couleur;
    EtatCase::Etat m_etat;


};


#endif // CASES_H
