#include "Procedures.h"


Procedures::Procedures(std::string proc)
    :m_proc(proc)
{
}

Procedures::~Procedures()
{}

//PERMET D'EXECUTER TOUTES LES ACTION CONTENU DANS LE VECTEUR D'ACTION//
void Procedures::executerAction()
{
    for (auto i : m_actions)
    {
        i -> executerAction();
    }
}

//PERMET D'AJOUTER DES ACTION DANS LE VECTEUR D'ACTIONS//
void Procedures::ajouterAction(Action *action)
{
    m_actions.push_back(action);
}

//PERMET DE SUPPRIMER UNE ACTION DU VECTEUR D'ACTION//
void Procedures::supprimerAction(int i)
{
    m_actions.erase(m_actions.cbegin() + i);//supprimer une action dans le vecteur en partant du debut du vecteur et en y ajoutant un entier passé en paramètre
}

