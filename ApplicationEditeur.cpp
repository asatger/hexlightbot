#include "ApplicationEditeur.h"
#include "Menu.h"

namespace {
const int W = 1280,
H = 720;
}

ApplicationEditeur::ApplicationEditeur()
    :Application{W, H, L"Jeu"}
{
    m_plateau = new Plateau();
    m_plateau -> chargerPlateau_Editeur();

    m_tex_background.loadFromFile("Sprites/Background.png"); //on charge l'image de fond
    m_background.setTexture(m_tex_background);
    m_background.setPosition({0,0});

    m_tex_instructions.loadFromFile("Sprites/Instructions.png");
    m_instructions.setTexture(m_tex_instructions);
    m_instructions.setPosition({900, 50});

    m_boutons.push_back(new Boutons("Sprites/Retour.png",   { 10, 10})); //on push les boutons d'interactions dans le vecteur de boutons
    m_boutons.push_back(new Boutons("Sprites/Reset.png",    { 114, 10}));
    m_boutons.push_back(new Boutons("Sprites/Save.png",     { 950, H/2 - 130/2}));

    m_etatCase = EtatCase::NONACCESSIBLE; //on initialise l'etat de la case
    m_numNiveau = 1; //on initialise le numero du premier fichier qui va etre enregistré
}

ApplicationEditeur::~ApplicationEditeur()
{
    delete m_plateau;
    for (auto i : m_boutons)
        delete i;
}

void ApplicationEditeur::loop()
{
    m_window.draw(m_background);
    m_window.draw(m_instructions);
    dessiner_boutons();
    m_plateau -> afficherPlateau(m_window);
    m_window.display();
}

void ApplicationEditeur::reset()
{
    m_plateau -> ~Plateau();
    m_plateau = new Plateau();
    m_plateau -> chargerPlateau_Editeur();
}

//PERMET DE DESSINER LES BOUTONS D'INTERACTIONS//
void ApplicationEditeur::dessiner_boutons()
{
    sf::Vector2f mouse(sf::Mouse::getPosition(m_window));
    for (auto i : m_boutons)
    {
        i -> afficher_bouton(m_window);
        if (i -> souris_dans_bouton(mouse))
            i -> changer_couleur_bouton(sf::Color(170,170,170));
        else
            i -> changer_couleur_bouton(sf::Color(255,255,255));
    }
}

void ApplicationEditeur::mouse_button_pressed()
{
    sf::Vector2f mouse(sf::Mouse::getPosition(m_window));

    int i,j;
    posToIndice(mouse,i,j); //on recuperere les indice correspondant a la position de la souris

    switch (m_etatCase) {//on test l'etat de la case
    case 1:
        m_etatCase = EtatCase::ACCESSIBLE;
        break;
    case 0:
        m_etatCase = EtatCase::ALLUMABLE;
        break;
    case 2:
        m_etatCase = EtatCase::NONACCESSIBLE;
        break;
    default:
        break;
    }

    m_plateau -> modifierPlateau_Editeur(m_etatCase, mouse); //on modifie le plateau en fonction de l'etat de la case et de la position de la souris

    if (m_boutons[0] -> souris_dans_bouton(mouse))
    {
        m_window.close();
        Menu menu;
        menu.run();
    }
    else if (m_boutons[1] -> souris_dans_bouton(mouse))
    {
        reset();
    }
    else if (m_boutons[2] -> souris_dans_bouton(mouse))
    {
        m_plateau -> sauvegarderPlateau(m_numNiveau);
        m_numNiveau++;
    }
}

void ApplicationEditeur::mouse_button_released()
{}

void ApplicationEditeur::mouse_moved()
{}

//PERMET DE BOUGER LE PERSONNAGE SUR LE PLATEAU//
void ApplicationEditeur::key_pressed(const sf::Event::KeyEvent &event)
{
    switch (event.code) {
    case sf::Keyboard::Q:
        m_plateau->deplacerPerso_Editeur("Q");//permet de deplacer le personnage vers la gauche
        break;
    case sf::Keyboard::D:
        m_plateau->deplacerPerso_Editeur("D");//permet de deplacer le personnage vers la droite
        break;
    case sf::Keyboard::Up:
        m_plateau->deplacerPerso_Editeur("Up");//permet de faire le personnage avancer
        break;
    default:
        break;
    }
}
