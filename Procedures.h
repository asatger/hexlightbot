#ifndef PROCEDURES_H
#define PROCEDURES_H

#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>

#include "Action.h"

using Position = sf::Vector2f;

class Procedures : public Action
{
public:
    Procedures(std::string proc);
    ~Procedures() override;
    void executerAction() override;
    void ajouterAction(Action * action) override;
    void supprimerAction(int i ) override;

private:
    std::vector<Action *> m_actions;
    std::string m_proc;


};

#endif // PROCEDURES_H
