#ifndef UTILITAIRES_H
#define UTILITAIRES_H

#include <iostream>
#include <SFML/Graphics.hpp>
#include "math.h"
#include "Cases.h"

using Position = sf::Vector2f;

void posToIndice(Position pos, int &i, int &j);
Position indiceToPos(const int &i, const int &j);
void posToIndiceAction(Position pos, int &j);
EtatCase::Etat toEnum (const std::string &element);
float module(const Position & v);
float distance(const Position & p1, const Position & p2 );
bool souris_dans_rond(const Position & mouse, const Position & position);
Cases *sourisDansCase(std::vector <Cases *> cases, Position &pos);

#endif // UTILITAIRES_H
