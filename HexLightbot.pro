TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS += -std=c++11 -pedantic -Wall -Wextra

LIBS           += -lsfml-graphics -lsfml-window -lsfml-system

SOURCES += main.cpp \
    Application.cpp \
    ApplicationJeu.cpp \
    ApplicationEditeur.cpp \
    Plateau.cpp \
    Menu.cpp \
    Cases.cpp \
    Boutons.cpp \
    Action.cpp \
    Procedures.cpp \
    Personnage.cpp \
    Utilitaires.cpp \
    Avancer.cpp \
    Allumer.cpp \
    Tourner.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    Application.h \
    ApplicationJeu.h \
    ApplicationEditeur.h \
    Plateau.h \
    Menu.h \
    Cases.h \
    Boutons.h \
    Action.h \
    Procedures.h \
    Personnage.h \
    Utilitaires.h \
    Avancer.h \
    Allumer.h \
    Tourner.h \
    EtatCase.h

OTHER_FILES += \
    Levels/1 \
    Levels/2

