#include "Utilitaires.h"
#include "Action.h"

namespace{
const Position COIN_TABLEAU {200,160};
const Position DIMS_CASE    {70, 80};
const Position COIN_ACTIONS{10,620},
DIMS_ACTIONS    {60, 60};
const int ECART = 10;
const int RAYON = 40;
}

//PERMET DE CONVERTIR UNE POSITION EN INDICE//
void posToIndice(Position pos, int &i, int &j)
{
    i = (pos.y - COIN_TABLEAU.y)/DIMS_CASE.y;//on calcule l'indice grâce à cette formule
    j = (pos.x - COIN_TABLEAU.x)/DIMS_CASE.x;
}

//PERMET DE CONVERTIR UN INDICE EN POSITION//
Position indiceToPos(const int &i, const int &j)
{
    if (j%2 == 0) //si j est pair
    {
        return {COIN_TABLEAU.x + DIMS_CASE.x * j, COIN_TABLEAU.y + DIMS_CASE.y * i };//on calcule la position grâce à cette formule
    }
    else
    {
        return {COIN_TABLEAU.x + DIMS_CASE.x * j, COIN_TABLEAU.y + DIMS_CASE.y * i + DIMS_CASE.y/2 };
    }
}

//PERMET DE CONVERTIR UNE POSITION EN INDICE POUR LE TABLEAU D'ACTION GRAPHIQUE//
void posToIndiceAction(Position pos, int &j)
{
    j = (pos.x - COIN_ACTIONS.x)/(DIMS_ACTIONS.x + ECART);
}

//PERMET DE CALCULER LE MODULE D'UNE POSITION//
float module(const Position & v)//source: IUT
{
    return sqrt(v.x * v.x + v.y * v.y);
}

//PERMET DE CALCULER LA DISTANCE ENTRE DEUX POSITION//
float distance(const Position & p1, const Position & p2 )//source: IUT
{
    return module( p1 - p2 );
}

//PERMET DE TESTER SI LA SOURIS EST DANS UN ROND//
bool souris_dans_rond(const Position & mouse, const Position & position)
{
    return distance(mouse, position) <= RAYON;
}

//PERMET DE CONVERTIR UNE CHAINE DE CARATERE EN ENUM//
EtatCase::Etat toEnum(const std::string &element)
{
    EtatCase::Etat e;
    if (element == "NONACCESSIBLE")
    {
        e = EtatCase::NONACCESSIBLE;
    }

    else if (element == "ACCESSIBLE")
    {
        e = EtatCase::ACCESSIBLE;
    }

    else if (element == "ALLUMABLE")
    {
        e = EtatCase::ALLUMABLE;
    }
    return e;
}

//PERMET DE RENVOYER LA CASE DANS LAQUELLE SE TROUVE LA POSITION DE LA SOURIS//
Cases *sourisDansCase(std::vector<Cases *> cases, Position &pos)
{
    Cases *c;
    for (auto elem : cases)
        if (souris_dans_rond(pos, elem -> getPos()))
            c = elem;
    return c;
}
