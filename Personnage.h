#ifndef PERSONNAGE_H
#define PERSONNAGE_H

#include <SFML/Graphics.hpp>
#include <iostream>

#include "Utilitaires.h"
#include "Cases.h"

using Position = sf::Vector2f;

class Plateau;

class Personnage
{
public:
    //FONCTIONS MEMBRES//
    Personnage(Position position, std::string nomFichier, int rotation);
    void afficher_perso(sf::RenderWindow &window);
    void avancer(Plateau *plateau);
    void tournerADroite();
    void tournerAGauche();

    //FONCTIONS GETTER//
    int getRotation();
    Position getPos();

    //FONCTIONS SETTER//
    void setPos(Position p);

private:
    sf::Sprite m_sprite;
    sf::Texture m_tex_sprite;
    Position m_position;
    std::string m_nomFichier;
    int m_rotation;
    enum Directions {HAUT, HAUTDROIT, HAUTGAUCHE, BASGAUCHE, BASDROIT, BAS} m_directions;
};

#endif // PERSONNAGE_H
