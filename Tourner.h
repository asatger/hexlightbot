#ifndef TOURNER_H
#define TOURNER_H

#include "Action.h"
#include "Personnage.h"

class Tourner : public Action
{
public:
    Tourner(std::string direction, Personnage * perso);
    ~Tourner() override;
    void executerAction() override;
private:
    std::string m_direction;
    Personnage * m_perso;
};

#endif // TOURNER_H
