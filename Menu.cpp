#include "Menu.h"
namespace {
constexpr unsigned int W = 1280, H = 720;
constexpr float RAYON = 20.0;
const Position TAILLE_BOUTONS {213, 130};

}

Menu::Menu()
    :Application {W, H, L"Menu"}
{
    m_tex_logo.loadFromFile("Sprites/Logo.png");
    m_logo.setTexture(m_tex_logo);
    m_logo.setPosition(sf::Vector2f(W/2 - m_logo.getTextureRect().width/2, 10));

    m_boutons.push_back(new Boutons("Sprites/PlayMenu.png", { W /2 - TAILLE_BOUTONS.x /2, TAILLE_BOUTONS.y * 3 }));
    m_boutons.push_back(new Boutons("Sprites/Quit.png",     { W /4 - TAILLE_BOUTONS.x /2, TAILLE_BOUTONS.y * 3 }));
    m_boutons.push_back(new Boutons("Sprites/Editing.png",  { W /2 + TAILLE_BOUTONS.x, TAILLE_BOUTONS.y * 3 }));
}

Menu::~Menu()
{
    for(auto i : m_boutons)
    {
        if(!nullptr)
            delete i;
    }
}

void Menu::loop()
{
    m_window.clear(sf::Color(206, 246, 245));

    m_window.draw(m_logo);
    dessiner_boutons();

    sf::Vector2f mouse(sf::Mouse::getPosition(m_window));

    for( auto i : m_boutons)
    {
        if (i -> souris_dans_bouton(mouse))
            i -> changer_couleur_bouton(sf::Color(170,170,170));
        else
            i -> changer_couleur_bouton(sf::Color(255,255,255));
    }

    m_window.display();
}

//PERMET DE DESSINER LES BOUTONS D'INTERACTIONS DU MENU//
void Menu::dessiner_boutons()
{
    for (auto i : m_boutons)
    {
        i->afficher_bouton(m_window);
    }
}

void Menu::mouse_button_pressed()
{
    sf::Vector2f mouse(sf::Mouse::getPosition(m_window));

    if (m_boutons[0] -> souris_dans_bouton(mouse))
    {
        m_window.close();
        ApplicationJeu appli;
        appli.run();
    }
    else if (m_boutons[1] -> souris_dans_bouton(mouse))
    {
        stop();
    }
    else if (m_boutons[2] -> souris_dans_bouton(mouse))
    {
        m_window.close();
        ApplicationEditeur editeur;
        editeur.run();
    }
}
