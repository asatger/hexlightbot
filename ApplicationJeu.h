#ifndef APPLIDEPLACEMENTPION_H
#define APPLIDEPLACEMENTPION_H

#include "Application.h"
#include "Plateau.h"
#include "Boutons.h"
#include "Utilitaires.h"

namespace {
const int nbrMAX = 6;
const int maxMAIN = 12;

}

using TBoutActions = std::array< Boutons * , nbrMAX>;

class ApplicationJeu : public Application
{
public:
    ApplicationJeu();
    ~ApplicationJeu();

private:
    Position m_position;
    Plateau * m_plateau;
    enum class Etat { INITIAL, DEPLACEMENT, FINAL, AJOUT, EXECUTION, SUPRESSION, FINI} m_etat;
    void reset(int i);
    int m_numNiveau;
    void loop()  override;


    //ELEMENTS GRAPHIQUES//
    std::vector <Boutons *> m_actions_main, m_actions_p1, m_actions_p2, m_boutons, m_boutons_actions;
    Boutons * m_courant;
    sf::Sprite m_background, m_main, m_proc1, m_proc2;
    sf::Texture m_tex_background, m_tex_main, m_tex_proc1, m_tex_proc2;
    TBoutActions m_actions;
    int m_indiceAction;
    int m_nbrColonneMain;
    int m_nbrLigneMain;
    int m_nbrColonneP1;
    int m_nbrLigneP1;
    int m_nbrColonneP2;
    int m_nbrLigneP2;

    void chargerSprite(sf::Texture &tex, sf::Sprite &sprite, float posX, float posY, std::string destination);
    void ajouterAction(int &nbrColonne, int &nbrLigne, int numAction, std::vector<Boutons*> &action, sf::Sprite sprite);
    void supprimerAction(std::vector<Boutons *> &actions, int &cpt, int numAction, int &nbrColonne, int &nbrLigne);
    void dessiner_boutons();
    void dessiner_boutons_actions();
    void charger_boutons_actions();

    //FONCTIONS EVENEMENTS//
    void mouse_button_pressed()                        override;
    void mouse_button_released()                       override;
    void mouse_moved()                                 override;
    //void key_pressed(const sf::Event::KeyEvent &event) override;
};

#endif // APPLIDEPLACEMENTPION_H
