#include <iostream>

#include "ApplicationJeu.h"
#include "ApplicationEditeur.h"
#include "Menu.h"

#include <SFML/Graphics.hpp>

using namespace sf;
using namespace std;

int main()
{
    Menu m;
    m.run();
    return 0;
}

