#ifndef ACTION_H
#define ACTION_H

#include <SFML/Graphics.hpp>
#include <iostream>

#include "Utilitaires.h"

using Position = sf::Vector2f;

class Action
{    
public:
    Action();
    virtual ~Action();
    virtual void executerAction() = 0;
    virtual void ajouterAction(Action *action);
    virtual void supprimerAction(int i);

private:


};

#endif // ACTION_H
