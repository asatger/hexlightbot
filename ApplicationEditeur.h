#ifndef APPLICATIONEDITEUR_H
#define APPLICATIONEDITEUR_H

#include "Application.h"
#include "Plateau.h"
#include "Boutons.h"
#include "Utilitaires.h"

class ApplicationEditeur : public Application
{

public:
    ApplicationEditeur();
    ~ApplicationEditeur();

private:
    Plateau *m_plateau;
    void loop() override;
    EtatCase::Etat m_etatCase;
    enum EtatEditeur {INITIAL, AVANCER, TOURNERD, TOURNERG} m_etatEditeur;
    void reset();
    int m_numNiveau;

    //ELEMENTS GRAPHIQUES//
    sf::Sprite m_background, m_instructions;
    sf::Texture m_tex_background, m_tex_instructions;
    std::vector <Boutons *> m_boutons;
    void dessiner_boutons();

    //FONCTIONS EVENEMENTS//
    void mouse_button_pressed()                        override;
    void mouse_button_released()                       override;
    void mouse_moved()                                 override;
    void key_pressed(const sf::Event::KeyEvent &event) override;
};

#endif // APPLICATIONEDITEUR_H
