#include "Boutons.h"

Boutons::Boutons(std::string nomFichier , Position pos)
    :m_nomFichier(nomFichier),
      m_pos(pos)
{
    m_tex.loadFromFile(nomFichier);
    m_sprite.setTexture(m_tex);
    m_sprite.setPosition(pos);
    m_sprite.setOrigin(0,0);

}

Boutons::~Boutons()
{}

//PERMET DE TESTER SI LES COORDONNEES DE LA SOURIS SONT DANS UN BOUTON//
bool Boutons::souris_dans_bouton(Position &mouse) const
{
    return (m_sprite.getGlobalBounds().contains(mouse));
}

//PERMET DE CAHNGER LA COULEUR D'UN BOUTON LORSQUE LA SOURIS PASSE DESSUS//
void Boutons::changer_couleur_bouton(sf::Color couleur)
{
    m_sprite.setColor(couleur);
}

//PERMET D'AFFICHER UN BOUTON//
void Boutons::afficher_bouton(sf::RenderWindow &window) const
{
    window.draw(m_sprite);
}

//PERMET DE DEPLACER UN BOUTON EN FONCTION D'UNE POSITION DONNEE//
void Boutons::deplacer_boutons(Position &pos)
{
    m_sprite.setPosition({pos.x - m_sprite.getTextureRect().width/2, //avec ce calcule on obtient le centre du sprite
                          pos.y - m_sprite.getTextureRect().height/2});
}

//PERMET DE MODIFIER LA POSITION D'UN SPRITE//
void Boutons::setPosition(const Position &pos)
{
    m_sprite.setPosition(pos);
}

//PERMET DE MODIFIER LE CHEMIN D'ACCES A UN SPRITE//
void Boutons::setString(std::string fic)
{
    m_tex.loadFromFile(fic);
}

//PERMET DE RECUPERER LE CHEMIN D'ACCES D'UN SPRITE//
std::string Boutons::getString()
{
    return m_nomFichier;
}

//PERMET D'OBTENIR LA POSITION D'UN SPRITE D'UN BOUTON//
Position Boutons::getPosition()
{
    return m_pos;
}
