#include "Tourner.h"

Tourner::Tourner(std::string direction, Personnage *perso)
    :m_direction(direction),
      m_perso(perso)
{}

Tourner::~Tourner()
{}

//EXECUTE L'ACTION TOURNER//
void Tourner::executerAction()
{
    if (m_direction == "Droite")
        m_perso->tournerADroite();
    else
        m_perso->tournerAGauche();
}

