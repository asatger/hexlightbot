#ifndef ALLUMER_H
#define ALLUMER_H

#include "Action.h"
#include "Plateau.h"

class Personnage;

class Allumer : public Action
{
public:
    Allumer(Personnage *perso, Plateau *plateau);
    ~Allumer() override;
    void executerAction() override;

private:
    Personnage *m_perso;
    Plateau *m_plateau;
};

#endif // ALLUMER_H
