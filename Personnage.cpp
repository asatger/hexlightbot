#include "Personnage.h"
#include "Plateau.h"

namespace {
const float RAYON = 20;
const Position COIN_TABLEAU {200,160};
const Position DIMS_CASE    {70, 80};
}

Personnage::Personnage(Position position, std::string nomFichier, int rotation):
    m_position(position),
    m_nomFichier(nomFichier),
    m_rotation(rotation)
{
    m_tex_sprite.loadFromFile(nomFichier);
    m_sprite.setTexture(m_tex_sprite);
    m_sprite.setPosition(position);
    m_sprite.setOrigin(RAYON, RAYON);
    m_sprite.setRotation(m_rotation);
    m_directions = BASDROIT;
}

//PERMET D'AFFICHER UN PERSONNAGE//
void Personnage::afficher_perso(sf::RenderWindow &window)
{
    window.draw(m_sprite);
}

//PERMET DE MODIFIER LA POSITION DU PERSONNAGE AFIN DE LA FAIRE AVANCER//
void Personnage::avancer(Plateau *plateau)
{
    Position p;
    int i, j;
    posToIndice(this->getPos(), i, j);//on recupere les indice correspondant a la position du personnage
    Position init = indiceToPos(i,j);//on note sa positio initiale

    switch (m_directions) {//on indique chaque deplacement à effectuer en fonction des indices d'un tableau et de la direction du personnage
    case HAUT:
        i --;
        break;
    case HAUTDROIT:
        if(j%2!=0)//permet de tester si j est impair
        {
            j++;
        }
        else
        {
            i--;
            j++;
        }
        break;
    case BASDROIT:
        if(j%2!=0)
        {
            i++;
            j++;
        }
        else
        {
            j ++;
        }

        break;
    case BAS:
        i++;
        break;
    case BASGAUCHE:
        if (j%2!=0)
        {
            i++;
            j--;
        }
        else
        {
            j --;
        }

        break;
    case HAUTGAUCHE:
        if(j%2 != 0)
        {
            j --;
        }

        else
        {
            i--;
            j--;
        }

        break;
    default:
        break;
    }
    if (plateau -> caseAccessible(i, j) && i >= 0 && i < 4 && j >= 0 && j <= 7) //on teste si la futur case à atteindre est accessible et si elle respecte la taille du plateau
    {
        p = indiceToPos(i,j);//on note la nouvelle position calculee a partir des nouveaux indices
        m_position = p;//on attribue cette nouvelle position à la positon de l'objet
        m_sprite.setPosition(m_position);//on change la position du sprite
    }
    else
    {
        m_position = init;//si les conditions ne sont pas satisfaites le personnage reste a sa positon
        m_sprite.setPosition(m_position);
    }
    i = j = 0;

}

//PERMET DE MODIFIER LA DIRECTION DU PERSONNAGE AFIN DE LE FAIRE TOURNER A DROITE//
void Personnage::tournerADroite()
{
    switch (m_directions) {//on indique les directions possible en fonction de la direction actuelle
    case HAUT:
        m_rotation = m_rotation + 60;//affecte la valeur de la nouvelle direction
        m_directions = HAUTDROIT;//on change l'etat de sa direction
        break;
    case HAUTDROIT:
        m_rotation = m_rotation + 60;
        m_directions = BASDROIT;
        break;
    case BASDROIT:
        m_rotation = m_rotation + 60;
        m_directions = BAS;
        break;
    case BAS:
        m_rotation = m_rotation + 60;
        m_directions = BASGAUCHE;
        break;
    case BASGAUCHE:
        m_rotation = m_rotation + 60;
        m_directions = HAUTGAUCHE;
        break;
    case HAUTGAUCHE:
        m_rotation = m_rotation + 60;
        m_directions = HAUT;
        break;
    default:
        break;
    }
    m_sprite.setRotation(m_rotation);//on change la rotation du sprite
}

//PERMET DE MODIFIER LA DIRECTION DU PERSONNAGE POUR LE FAIRE TOURNER A GAUCHE//
void Personnage::tournerAGauche()
{
    switch (m_directions) {//même principe que la fonction précédente
    case HAUT:
        m_rotation = m_rotation - 60;
        m_directions = HAUTGAUCHE;
        break;
    case HAUTGAUCHE:
        m_rotation = m_rotation - 60;
        m_directions = BASGAUCHE;
        break;
    case BASGAUCHE:
        m_rotation = m_rotation - 60;
        m_directions = BAS;
        break;
    case BAS:
        m_rotation = m_rotation - 60;
        m_directions = BASDROIT;
        break;
    case BASDROIT:
        m_rotation = m_rotation - 60;
        m_directions = HAUTDROIT;
        break;
    case HAUTDROIT:
        m_rotation = m_rotation - 60;
        m_directions = HAUT;
        break;
    default:
        break;
    }
    m_sprite.setRotation(m_sprite.getRotation() - 60);
}

//PERMET D'ACCEDER A LA POSITON DU PERSONNAGE EN DEHORS DE LA CLASSE//
Position Personnage::getPos()
{
    return m_position;
}

//PERMET DE MODIFIER LA POSITION DU PERSONNAGE EN DEHORS DE LA CLASSE//
void Personnage::setPos(Position p)
{
    m_position = p;
}

//PERMET D'ACCEDER A LA ROTATION DU PERSONNAGE EN DEHORS DE LA CLASSE//
int Personnage::getRotation()
{
    return m_rotation;
}


