#ifndef AVANCER_H
#define AVANCER_H

#include "Action.h"
#include "Plateau.h"

class Avancer : public Action
{
public:
    Avancer(Plateau *plateau);
    ~Avancer() override;
    void executerAction() override;

private:
    Plateau  *m_plateau;
};

#endif // AVANCER_H
