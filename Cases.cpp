#include "Cases.h"

namespace {
const int POINTS = 6;
const int ROTATION = 30;
const int RAYON = 40;
const sf::Color CASE_NACC(191,201,219),
CASE_ACC(67,156,207),
CASE_ALL(243,237,158),
CASE_VIS(7,106,157);
}

Cases::Cases(Position position, EtatCase::Etat etat):
    m_position(position),
    m_etat(etat)
{
    m_hexa.setPosition(m_position);
    m_hexa.setRadius(RAYON);
    m_hexa.setOrigin(RAYON, RAYON);
    m_hexa.setPointCount(POINTS);
    m_hexa.setRotation(ROTATION);
    m_hexa.setOutlineThickness(3.);
    m_hexa.setOutlineColor(sf::Color(50,50,50,150));
}

Cases::~Cases()
{}

//PERMET D'AFFICHER UNE CASE EN FONCTION DE SON ETAT//
void Cases::afficher_case(sf::RenderWindow &window)
{
    if (m_etat == EtatCase::ACCESSIBLE) //on change la couleur de l'hexagone en fonction de l'etat de la case
        m_hexa.setFillColor(CASE_ACC);
    else if(m_etat == EtatCase::NONACCESSIBLE)
        m_hexa.setFillColor(CASE_NACC);
    else if(m_etat == EtatCase::ALLUMABLE)
        m_hexa.setFillColor(CASE_ALL);
    else if(m_etat == EtatCase::ALLUMEE)
        m_hexa.setFillColor(sf::Color::Yellow);
    else if(m_etat == EtatCase::VISITEE)
        m_hexa.setFillColor(CASE_VIS);


    window.draw(m_hexa); //on dessine la case
}

//PERMET DE MODIFIER L'ETAT D'UN CASE AFIN DE L'ALLUMER//
void Cases::allumerCase()
{
    if (m_etat == EtatCase::ALLUMABLE)
    {
        m_etat = EtatCase::ALLUMEE;
    }
}

//PERMET DE MARQUER UNE CASE UNE FOIS QUE LE PERSONNAGE EST PASSE DESSUS EN CHANGEANT SON ETAT//
void Cases::estVisitee()
{
    m_etat = EtatCase::VISITEE;
}

//PERMET DE MODIFIER L'ETAT D'UNE CASE EN DEHORS DE LA CLASSE//
void Cases::setEtat(EtatCase::Etat etat)
{
    m_etat = etat;
}

//PERMET D'OBTENIR LA POSITION D'UNE CASE EN DEHORS DE LA CLASSE//
Position Cases::getPos()
{
    return m_position;
}

//PERMET D'OBTENIR L'ETAT D'UNE CASE EN DEHORS DE LA CLASSE//
EtatCase::Etat Cases::getEtat()
{
    return m_etat;
}

//PERMET D'OBTENIR L'HEXAGONE EN DEHORS DE LA CLASSE//
sf::CircleShape Cases::getCircle()
{
    return m_hexa;
}
