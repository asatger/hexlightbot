#include "Avancer.h"


Avancer::Avancer(Plateau *plateau)
    :m_plateau(plateau)
{}

Avancer::~Avancer()
{}

//PERMET L'EXECUTION DE L'ACTION PERMETANT DE FAIRE AVANCER LE PERSONNAGE//
void Avancer::executerAction()
{
    m_plateau->avancerPersonnage();
}
