#include "Plateau.h"
#include "Procedures.h"
#include "Avancer.h"
#include "Allumer.h"
#include "Tourner.h"

namespace{
const Position COIN_TABLEAU {200,160};
const Position DIMS_CASE    {70, 80};
const float DIAMETRE = 40;
const sf::Color CASE_NACC(191,201,219),
CASE_ACC(67,156,207),
CASE_ALL(243,237,158);
}

Plateau::Plateau()
{}

Plateau::~Plateau()
{
    for (auto i : m_cases)
        if(i!=nullptr)
            delete i;

    if(!nullptr)
        for (int i = 0; i < nbrAction; i++)
            delete m_actions[i];

    delete m_perso;
}

//PERMET DE CHARGER LE PLATEAU DU MENU EDITION//
void Plateau::chargerPlateau_Editeur()
{
    for (int i = 0; i < rowMAX; i ++)
        for (int j = 0; j < colMAX; j++)
        {
            m_plateau[i][j] = EtatCase::NONACCESSIBLE;//on initialise toutes les cases du plateau à l'etat NONACCESSIBLE
            m_cases.push_back(new Cases(indiceToPos(i, j), EtatCase::NONACCESSIBLE));//on ajoute une cases dans le vecteur cases à une position definie a partir des indices du plateau et prenant l'etat de la case
        }
    m_perso = new Personnage(indiceToPos(0,0), "Sprites/Personnage.png", 30);//on donne au personnage une position initiale

    for (int elem = 0; elem < nbrAction; elem++)
        m_actions[elem]= nullptr;//on initialise le vecteur d'action à null
}

//PERMET DE MODIFIER LE PLATEAU EDITÉ PAR L'UTILISATEUR//
void Plateau::modifierPlateau_Editeur(EtatCase::Etat etat, Position &mouse)
{
    Cases *c;
    int i, j;
    c = sourisDansCase(m_cases, mouse);//on recupère la case correspondant a la position de la souris
    c -> setEtat(etat);//la case correspondante prend l'etat passé en parametre
    posToIndice(c->getPos(), i, j);//on recupere les indices correspondant à la position de la case
    if (i >= 0 && i < rowMAX && j >= 0 && j < colMAX)//on verifie si les indices correspondent à une case d'un plateau
        m_plateau[i][j] = c -> getEtat();//on modifie l'etat de cette case dans le plateau
}

//PERMET DE DEPLACER ET TOURNER LE PERSONNAGE EN MODE EDITION//
void Plateau::deplacerPerso_Editeur(std::string touche)
{
    if (touche == "Q")
        m_perso -> tournerAGauche();
    else if (touche == "D")
        m_perso -> tournerADroite();
    else if (touche == "Up")
        m_perso->avancer(this);
}

//PERMET LA SAUVEGARDE DU PLATEAU CRÉÉ EN MODE EDITION DANS UN FICHIER TEXTE//
void Plateau::sauvegarderPlateau(int numNiveau)
{    
    //Code inspiré de la page stackoverflow/questions/1121383 -->
    int file_count = 0;
    DIR * dirp;
    struct dirent * entry;
    dirp = opendir("LevelsEditeur/");
    while ((entry = readdir(dirp)) != NULL) {
        if (entry->d_type == DT_REG) {
            file_count++;//incremente file_count à chaque fois qu'un fichier est trouvé dans le repertoire indiqué
        }
    }
    closedir(dirp);
    // <-- Code inspiré de la page stackoverflow/questions/1121383

    if (numNiveau <= file_count)
        numNiveau = file_count + 1;//permet de sauvegarder plusieurs niveaux en fonction du nombre de fichier contenu dans le repertoire

    std::ofstream fichier("LevelsEditeur/" + std::to_string(numNiveau), std::ios::out);//on ouvre un fichier avec la nouvelle valeur de numNiveau
    std::string etat;
    int i, j, rotation;
    if (fichier)
    {
        for (i = 0; i < rowMAX; i++)
        {
            for(j = 0; j < colMAX; j++)
            {
                if (m_plateau[i][j] == 0)
                    etat = "ACCESSIBLE";
                else if(m_plateau[i][j] == 1)
                    etat = "NONACCESSIBLE";
                else if(m_plateau[i][j] == 2)
                    etat = "ALLUMABLE";

                fichier << i << " " << j << " " << etat << std::endl; // on écrit les indices et l'état de la case dans le fichier
            }
            fichier << std::endl;
        }
        posToIndice(m_perso->getPos(), i, j);//on recupere les indice correspondant à la position du personnage
        rotation = m_perso->getRotation();//on recupere la rotation du personnage
        fichier << "perso " << i << " " << j << " " << rotation << std::endl;//puis on ajoute les idinces et la rotation
    }
}

//PERMET DE CHARGER UN NIVEAU A PARTIR D'UN FICHIER TEXTE//
void Plateau::chargerPlateau(int niveau)
{
    std::string destination;
    if (niveau <= 2)// le 2 correspond au nombre de niveau présent de base dans le jeu
        destination = "Levels/" + std::to_string(niveau);//on charge les niveaux de bases
    else
    {
        destination = "LevelsEditeur/" + std::to_string(niveau - 2 ); // on charge les fichiers du dossier editeur dès que niveau dépasse 2 on. niveau-2 permet d'incrementer le nom du fichier
    }
    std::ifstream fichier(destination, std::ios::in);//on ouvre le fichier correspondant

    if (fichier)
    {
        std::string elem;
        fichier >> elem;
        while (fichier.is_open())
        {
            int i = std::stoi(elem);//i prend la valeur du flux courant
            fichier >> elem;
            int j = std::stoi(elem);//j prend la valeur du flux courant
            fichier >> elem;
            m_plateau[i][j] = toEnum(elem);//on donne un etat à la case du plateau correspondante à i et j

            if (m_plateau[i][j] == EtatCase::NONACCESSIBLE)
                m_cases.push_back(new Cases(indiceToPos(i, j), EtatCase::NONACCESSIBLE));//permet de créer une case correspondante à la case du plateau en fonction de son etat

            else if (m_plateau[i][j] == EtatCase::ACCESSIBLE)
                m_cases.push_back(new Cases(indiceToPos(i, j), EtatCase::ACCESSIBLE));//idem

            else if (m_plateau[i][j] == EtatCase::ALLUMABLE)
                m_cases.push_back(new Cases(indiceToPos(i, j), EtatCase::ALLUMABLE));//idem

            fichier >> elem;
            if (elem == "perso")//on teste si le dernier element du fichier correspond au personnage
            {
                fichier >> elem;
                int i = std::stoi(elem);//on recupere ses indices
                fichier >> elem;
                int j = std::stoi(elem);
                fichier >> elem;
                int rotation = std::stoi(elem);//on recupere sa rotation initiale

                m_perso = new Personnage(indiceToPos(i, j), "Sprites/Personnage.png", rotation);//on creer un nouveau personnage à partir des inforamtions precedente
                fichier >> elem;
            }

            if (fichier.eof())
                fichier.close();//on ferme le fichier si c'est la fin
        }
    }
    else
        std::cout << "Impossible de trouver le fichier !" << std::endl;
}

//PERMET DE CHARGER LES DIFFERENTES ACTIONS QUI POURRONT ETRE UTILISÉES SUR LE PLATEAU//
void Plateau::chargerActions()
{
    m_actions[0] = new Avancer(this);
    m_actions[1] = new Tourner("Droite", m_perso);
    m_actions[2] = new Tourner ("Gauche", m_perso);
    m_actions[3] = new Allumer(m_perso, this);
    m_actions[4] = new Procedures("P1");
    m_actions[5] = new Procedures("P2");
    m_actions[6] = new Procedures("Main");
}

//PERMET D'AFFICHER LE PLATEAU DE JEU//
void Plateau::afficherPlateau(sf::RenderWindow & window)
{
    for ( auto elem : m_cases)
        elem->afficher_case(window);

    if (m_perso != nullptr)
        m_perso -> afficher_perso(window);
}

//PERMET DE FAIRE AVANCER LE PERSONNAGE EN MODE JEU//
void Plateau::avancerPersonnage()
{
    int i, j;
    posToIndice(recupererCasePerso(m_perso)-> getPos(),i,j);//on les indices de la case du personnage avant son deplacement
    if (m_plateau[i][j] != EtatCase::ALLUMABLE /*|| m_plateau[i][j] != EtatCase::VISITEE*/)//si la case du plateau n'est pas allumable ou deja visité
    {
        recupererCasePerso(m_perso)-> estVisitee();//on marque la case comme visitée
        m_plateau[i][j] = EtatCase::VISITEE;
    }
    m_perso->avancer(this);//on deplace ensuite le personnage vers sa nouvelle case
    posToIndice(recupererCasePerso(m_perso)-> getPos(),i,j);//on réitaire l'opération précédente mais avec la nouvelle case du personnage;
    if (m_plateau[i][j] != EtatCase::ALLUMABLE)
    {
        recupererCasePerso(m_perso)->estVisitee();
        m_plateau[i][j] = EtatCase::VISITEE;
    }
}

//PERMET DE VERIFIER SI UNE CASE EST ACCESSIBLE//
bool Plateau::caseAccessible(int &i, int &j)
{
    return (m_plateau[i][j] != EtatCase::NONACCESSIBLE);//on teste si la case du personnage est accessible
}

//PERMET D'AJOUTER UNE ACTION AU VECTEUR D'ACTIONS//
void Plateau::ajouterAction(int i, int numAction)
{
    m_actions[i] -> ajouterAction(m_actions[numAction]);//permet d'ajouter une action à partir du vecteur d'action du plateau a partir de l'indice i correspondant soit au main soit au proc1
                                                        //soit au proc2 et à partir d'un numero d'action correspondant au differents actions possibles
}

//PERMET DE SUPPRIMER UNE ACTION DU VECTEUR D'ACTION//
void Plateau::supprimerAction(int i, int numAction)
{
    m_actions[i] -> supprimerAction(numAction);//meme principe que la fonction précedente mais pour effacer une action du vecteur
}

//PERMET D'EXECUTER LE MAIN//
void Plateau::executer_Main()
{
    m_actions[6] -> executerAction();//pointeur d'action vers la fonction qui permet d'executer une action
}

//PERMET DE VERIFIER SI LE PLATEAU DE JEU EST COMPLET DONC SI LE NIVEAU EST FINI//
bool Plateau::plateauComplet()
{
    bool b;
    int cpt = 0;
    for (auto elem : m_cases)
    {
        if (elem -> getEtat() == EtatCase::ACCESSIBLE || elem -> getEtat() == EtatCase::ALLUMABLE)
            cpt++;//on increment le compteru a chaque fois qu'une case accessible ou allumable est rencontrée
    }
    if(cpt != 0)//si le compteur n'est pas null, cela signifie que le plateau contient encore des cases accessible ou allumable donc que le plateau n'est pas complet
        b = false;
    else
        b = true;
    return b;
}

//PERMET DE RECUPERER LA CASE SUR LAQUELLE LE PERSONNAGE SE SITUE//
Cases* Plateau::recupererCasePerso(Personnage *perso)
{
    Cases *c;
    for (auto i : m_cases)
        if (perso -> getPos() == i -> getPos())//on teste si la position du personnage est egale à la position d'une case du vecteur de cases
            c = i;
    return c;//on retourne cette case
}


