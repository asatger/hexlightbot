#ifndef BOUTONS_H
#define BOUTONS_H

#include <SFML/Graphics.hpp>
#include <iostream>

using Position = sf::Vector2f;

class Boutons
{
public:
    //CONSTRUCTEUR & DESTRUCTEUR//
    Boutons(std::string nomFichier, Position pos );
    ~Boutons();

    //FONCTIONS MEMBRES//
    bool souris_dans_bouton(Position &mouse) const;
    void changer_couleur_bouton (sf::Color couleur);
    void afficher_bouton(sf::RenderWindow & window) const;
    void deplacer_boutons(Position &mouse);

    //FONCTIONS SETTER//
    void setPosition(const Position &pos);
    void setString(std::string fic);

    //FONCTIONS GETTER//
    sf::IntRect getTextureRect();
    std::string getString();
    Position getPosition();

private:
    std::string m_nomFichier;
    Position m_pos;
    sf::Sprite m_sprite;
    sf::Texture m_tex;

};

#endif // BOUTONS_H
