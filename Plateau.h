#ifndef PLATEAU_H
#define PLATEAU_H

#include <SFML/Graphics.hpp>
#include <iostream>
#include <sstream>
#include <fstream>
#include <array>
#include <vector>
#include <map>
#include <stdlib.h>
#include <dirent.h>

#include "Utilitaires.h"
#include "Action.h"
#include "EtatCase.h"

class Personnage;
class Cases;

namespace {
const int rowMAX = 5,
          colMAX = 8,
          nbrAction = 7;
}

using Position = sf::Vector2f;
using TPlateau = std::array<std::array<int, colMAX>, rowMAX>;
using TActions = std::array< Action * , nbrAction>;

class Plateau
{
public:

    //CONSTRUCTEURS & DESTRUCTEURS//
    Plateau();
    ~Plateau();

    //FONCTIONS MEMBRES//
    void chargerPlateau_Editeur();
    void chargerPlateau(int niveau);
    void chargerActions();
    void afficherPlateau(sf::RenderWindow &window);
    void avancerPersonnage();
    bool caseAccessible(int &i, int &j);
    void ajouterAction(int i, int numAction);
    void supprimerAction(int i, int numAction);
    void executer_Main();
    bool plateauComplet();
    void modifierPlateau_Editeur(EtatCase::Etat etat, Position &mouse);
    void sauvegarderPlateau(int numNiveau);
    void deplacerPerso_Editeur(std::string touche);

    //FONCTIONS GETTER//
    sf::Sprite getSpritePerso();
    Cases *recupererCasePerso(Personnage * perso);

private:
    std::string m_nomNiveau;
    TPlateau m_plateau;
    std::vector<Cases *> m_cases;
    TActions m_actions;
    Personnage * m_perso;
    EtatCase m_etatCase;


};

#endif // PLATEAU_H
