#include "ApplicationJeu.h"
#include "Menu.h"

namespace {
const int W = 1280,
H = 720,
ECART = 10,
TMAIN = 12,
TP1 = 8,
TP2 = 8;
const Position COIN_ACTIONS{10,620},
DIMS_ACTIONS{60, 60};
}

ApplicationJeu::ApplicationJeu()
    : Application {W, H, L"Jeu"}
{
    m_numNiveau = 1;
    m_plateau = new Plateau();
    m_plateau->chargerPlateau(m_numNiveau);
    m_plateau->chargerActions();
    charger_boutons_actions();

    chargerSprite(m_tex_background, m_background, 0, 0, "Sprites/Background.png");
    chargerSprite(m_tex_main, m_main, 890, 100, "Sprites/Main.png");
    chargerSprite(m_tex_proc1, m_proc1, 890, 350, "Sprites/Proc1.png");
    chargerSprite(m_tex_proc2, m_proc2, 890, 532, "Sprites/Proc2.png");

    m_boutons.push_back(new Boutons("Sprites/Play.png",     { 1020, 30}));
    m_boutons.push_back(new Boutons("Sprites/Retour.png",   { 10, 10}));
    m_boutons.push_back(new Boutons("Sprites/Reset.png",    { 114, 10}));

    m_etat = Etat::INITIAL;
    m_nbrColonneMain = 0;
    m_nbrLigneMain = 0;
    m_nbrColonneP1 = 0;
    m_nbrLigneP1 = 0;
    m_nbrColonneP2 = 0;
    m_nbrLigneP2 = 0;
}

ApplicationJeu::~ApplicationJeu()
{
    for ( auto i : m_boutons)
        if(!nullptr)
            delete i;

    for (auto i : m_boutons_actions)
        if(!nullptr)
            delete i;

    for (int i = 0; i < nbrMAX; i++)
        delete m_actions[i];

    for(auto i : m_actions_main)
        if(!nullptr)
            delete i;

    for(auto i : m_actions_p1)
        if(!nullptr)
            delete i;

    for(auto i : m_actions_p2)
        if(!nullptr)
            delete i;

    delete m_plateau;
    delete m_courant;
}

//PERMET DE REINITIALISER LE PLATEAU ACTUEL//
void ApplicationJeu::reset(int i)
{
    m_plateau -> ~Plateau();
    m_plateau = new Plateau();
    m_plateau -> chargerPlateau(i);
    m_plateau -> chargerActions();
    m_actions_main.clear();
    m_actions_p1.clear();
    m_actions_p2.clear();
    m_nbrColonneMain = 0;
    m_nbrLigneMain = 0;
    m_nbrColonneP1 = 0;
    m_nbrLigneP1 = 0;
    m_nbrColonneP2 = 0;
    m_nbrLigneP2 = 0;
    m_etat = Etat::INITIAL;
}


void ApplicationJeu::loop()
{
    sf::Vector2f mouse(sf::Mouse::getPosition(m_window));

    if (m_etat == Etat::AJOUT)
    {
        m_courant = new Boutons (m_actions[m_indiceAction] -> getString(), mouse);
        m_courant -> setPosition({mouse.x - DIMS_ACTIONS.x/2, mouse.y - DIMS_ACTIONS.y/2});
    }
    if (m_etat == Etat::SUPRESSION)
    {
        m_courant -> setPosition({mouse.x - DIMS_ACTIONS.x/2, mouse.y - DIMS_ACTIONS.y/2});
    }

    m_window.draw(m_background);
    m_window.draw(m_main);
    m_window.draw(m_proc1);
    m_window.draw(m_proc2);


    m_plateau->afficherPlateau(m_window);
    dessiner_boutons();
    dessiner_boutons_actions();

    if(m_courant != nullptr)
        m_courant -> afficher_bouton(m_window);

    m_window.display();
}

//PERMET DE CHARGER LES SPRITES AFFICHES DANS LE MENU JEU//
void ApplicationJeu::chargerSprite(sf::Texture &tex, sf::Sprite &sprite, float posX, float posY, std::string destination)
{
    tex.loadFromFile(destination);
    sprite.setTexture(tex);
    sprite.setPosition({posX, posY});
}

//PERMET D'AJOUTER GRAPHIQUEMENT UNE ACTION DANS LE MAIN, LE PROC1 OU LE PROC2//
void ApplicationJeu::ajouterAction(int &nbrColonne, int &nbrLigne, int numAction, std::vector<Boutons *> &action, sf::Sprite sprite)
{
    if (nbrColonne == 4)//si on atteint 4 action dans le processus
    {
        nbrColonne = 0;//on retourne au debut
        nbrLigne ++;//et on passe a la ligne suivante
    }
    m_plateau -> ajouterAction(numAction, m_indiceAction);//on ajoute l'action glissée dans le vecteur d'action prêt à être éxecutée
    action.push_back(m_courant);//on ajouter le bouton courant dans le vecteur graphique
    m_courant -> setPosition( {sprite.getPosition().x + 25 + (25 + DIMS_ACTIONS.x) * nbrColonne, sprite.getPosition().y + 36 + (7  + DIMS_ACTIONS.y) * nbrLigne});// on donne une position au sprite
    nbrColonne++;
    m_courant = nullptr;//on supprime le bouton courant

}

//PERMET DE SUPPRIMER GRAPHIQUEMENT UNE ACTION DU MAIN, PROC1 OU PROC2//
void ApplicationJeu::supprimerAction(std::vector<Boutons *> &actions, int &cpt, int numAction, int &nbrColonne, int &nbrLigne)
{
    for (Boutons *i : actions)//on cherche pour chaque boutons du vecteur passé en paramètre s'il est égale au bouton courant
    {
        if(i == m_courant)
        {
            actions.erase(actions.cbegin() + cpt); //on supprime le boutons graphique
            m_plateau->supprimerAction(numAction, cpt); //on suppime l'action du vecteur d'action
            m_etat = Etat::INITIAL;
            nbrColonne --;

            if (nbrColonne < 0)//si nombre colonne negatif on revient en bout de ligne
            {
                nbrLigne--;
                nbrColonne = 3;
            }
            if (nbrLigne < 0)
            {
                nbrColonne = 0;
            }
        }
        cpt++;
    }
}

//PERMET DE DESSINER DES BOUTONS D'INTERACTIONS ET DE CHANGER LEUR COULEUR SI LA SOURIS EST DESSUS//
void ApplicationJeu::dessiner_boutons()
{
    sf::Vector2f mouse(sf::Mouse::getPosition(m_window));
    for (auto i : m_boutons)
    {
        i -> afficher_bouton(m_window);
        if (i -> souris_dans_bouton(mouse))
            i -> changer_couleur_bouton(sf::Color(170,170,170));
        else
            i -> changer_couleur_bouton(sf::Color(255,255,255));
    }
}

//PERMET DE DESSINER LES BOUTONS D'ACTIONS UNE FOIS PUSH DANS LES VECTEURS GRAPHIQUES DES PROCESSUS//
void ApplicationJeu::dessiner_boutons_actions()
{
    sf::Vector2f mouse(sf::Mouse::getPosition(m_window));
    for (int i = 0; i < nbrMAX; i++)
    {
        m_actions[i] -> afficher_bouton(m_window);
    }
    for (auto i : m_actions_main)
    {
        i -> afficher_bouton(m_window);
        if (i -> souris_dans_bouton(mouse))
            i -> changer_couleur_bouton(sf::Color(170,170,170));
        else
            i -> changer_couleur_bouton(sf::Color(255,255,255));
    }
    for (auto i : m_actions_p1)
    {
        i -> afficher_bouton(m_window);
        if (i -> souris_dans_bouton(mouse))
            i -> changer_couleur_bouton(sf::Color(170,170,170));
        else
            i -> changer_couleur_bouton(sf::Color(255,255,255));
    }
    for (auto i : m_actions_p2)
    {
        i -> afficher_bouton(m_window);
        if (i -> souris_dans_bouton(mouse))
            i -> changer_couleur_bouton(sf::Color(170,170,170));
        else
            i -> changer_couleur_bouton(sf::Color(255,255,255));
    }
}

//PERMET DE CREER LES BOUTONS QUI SERONT GLISSES DANS LES PROCESSUS//
void ApplicationJeu::charger_boutons_actions()
{
    int i = 0;
    int j = 0;
    m_actions[i] = new Boutons("Sprites/Avancer.png", {COIN_ACTIONS.x +(DIMS_ACTIONS.x + ECART) * i , COIN_ACTIONS.y + DIMS_ACTIONS.y * j }); //on creer un bouton en fonction de ses indices i et j
    i++;
    m_actions[i] = new Boutons("Sprites/Droite.png", {COIN_ACTIONS.x + (DIMS_ACTIONS.x + ECART) * i, COIN_ACTIONS.y + DIMS_ACTIONS.y * j });
    i++;
    m_actions[i] = new Boutons("Sprites/Gauche.png", {COIN_ACTIONS.x + (DIMS_ACTIONS.x + ECART) * i, COIN_ACTIONS.y + DIMS_ACTIONS.y * j });
    i++;
    m_actions[i] = new Boutons("Sprites/Allumer.png",{COIN_ACTIONS.x + (DIMS_ACTIONS.x + ECART) * i, COIN_ACTIONS.y + DIMS_ACTIONS.y * j });
    i++;
    m_actions[i] = new Boutons("Sprites/P1.png",  {COIN_ACTIONS.x + (DIMS_ACTIONS.x + ECART) * i, COIN_ACTIONS.y + DIMS_ACTIONS.y * j });
    i++;
    m_actions[i] = new Boutons("Sprites/P2.png", {COIN_ACTIONS.x + (DIMS_ACTIONS.x + ECART) * i, COIN_ACTIONS.y + DIMS_ACTIONS.y * j });

    m_courant = nullptr; //on initialise le vecteur courant qui nous servira a afficher l'action lors du déplacement
}


void ApplicationJeu::mouse_button_pressed()
{
    sf::Vector2f mouse(sf::Mouse::getPosition(m_window));

    if(m_boutons[0] -> souris_dans_bouton(mouse)) // si on clique sur le bouton play on execute les actions puis on verifie si le niveau est fini
    {
        m_plateau -> executer_Main();
        if (m_plateau->plateauComplet())
        {
            m_etat = Etat::FINI;
            m_numNiveau ++;
            reset(m_numNiveau);//on change de niveau
        }
    }
    else if(m_boutons[1] -> souris_dans_bouton(mouse))// si on clique sur le bouton retour, on revient au menu
    {
        m_window.close();
        Menu menu;
        menu.run();
    }
    else if(m_boutons[2] -> souris_dans_bouton(mouse))//si on clique sur le bouton reset, on reinitialise le niveau
    {
        reset(m_numNiveau);
    }

    if(m_etat == Etat::INITIAL)
    {
        for (int i = 0; i < nbrMAX; i ++)
        {
            if (m_actions[i] -> souris_dans_bouton(mouse))//si on clique sur les actions a deplacer on note son indice afin de creer le bouton lors du deplacement
            {
                m_indiceAction = i;
                m_etat = Etat::AJOUT;
            }
        }
        for (auto i : m_actions_main)
        {
            if (i -> souris_dans_bouton(mouse))//si on clique sur une action dans le processus main, le bouton courant prend la valeur du bouton selectionné
            {
                m_courant = i;
                m_etat = Etat::SUPRESSION;
            }
        }
        for (auto i : m_actions_p1)
        {
            if (i -> souris_dans_bouton(mouse))//idem
            {
                m_courant = i;
                m_etat = Etat::SUPRESSION;
            }
        }
        for (auto i : m_actions_p2)
        {
            if (i -> souris_dans_bouton(mouse))//idem
            {
                m_courant = i;
                m_etat = Etat::SUPRESSION;
            }
        }
    }
}

void ApplicationJeu::mouse_button_released()
{
    sf::Vector2f mouse(sf::Mouse::getPosition(m_window));

    if (m_etat == Etat::AJOUT)
    {

        if (m_main.getGlobalBounds().contains(mouse) && m_actions_main.size() < TMAIN) //si on relache dans le main et que la taille du vecteur est respectee
            ajouterAction(m_nbrColonneMain, m_nbrLigneMain, 6, m_actions_main, m_main);//on ajoute  l'action dans le processus

        else if (m_proc1.getGlobalBounds().contains(mouse) && m_actions_p1.size() < TP1)//idem
            ajouterAction(m_nbrColonneP1, m_nbrLigneP1, 4, m_actions_p1, m_proc1);


        else if (m_proc2.getGlobalBounds().contains(mouse) && m_actions_p2.size() < TP2)//idem
            ajouterAction(m_nbrColonneP2, m_nbrLigneP2, 5, m_actions_p2, m_proc2);

        else
        {
            m_courant = nullptr;//si relache le bouton en dehors des processus, on passe le bouton courant a null
            m_etat = Etat::INITIAL;
        }

        m_etat = Etat::INITIAL;
    }

    else if (m_etat == Etat::SUPRESSION)
    {
        int cpt = 0;
        if (!m_main.getGlobalBounds().contains(mouse) && !m_proc1.getGlobalBounds().contains(mouse) && !m_proc2.getGlobalBounds().contains(mouse)) //si on relache courant dans aucun processus
        {
            supprimerAction(m_actions_main, cpt, 6, m_nbrColonneMain, m_nbrLigneMain); //on supprime l'action selectionnee
            cpt = 0;
            supprimerAction(m_actions_p1, cpt, 4, m_nbrColonneP1, m_nbrLigneP1);//idem
            cpt = 0;
            supprimerAction(m_actions_p2, cpt, 5, m_nbrColonneP2, m_nbrLigneP2);//idem
            m_courant = nullptr;
        }
        m_etat = Etat::INITIAL;
    }
}

void ApplicationJeu::mouse_moved()
{}
